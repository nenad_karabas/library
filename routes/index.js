var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    var db = req.db;
    var collection = db.get('library');
    collection.find({},{},function(err,docs){
        res.render('library', {
            books: docs
        });
    });
});

router.get('/newbook', function(req, res) {
    console.log('newbook');
    res.render('newbook');
});

router.get('/editbook', function(req, res) {
    var db = req.db;
    var collection = db.get('library');
    console.log("EDIT " + req.param('id'));
    collection.find({_id:req.param('id')},{},function(err,docs){
        res.render("editbook", {
            books: docs
        });
    });
});

router.put('/updatebook', function(req, res) {
    console.log("pre " + req.param('id'));
    var db = req.db;
    var collection = db.get('library');
    collection.update({_id: req.param('id')}, {
            author: req.body.author,
            title: req.body.title
    },{},function (err, doc) {
        if (err) {
            res.send("Unable to update library");
        }
        else {
            res.redirect('/');
        }
    });
    
});
    
router.delete('/deletebook', function(req, res){
    var db = req.db;
    var collection = db.get('library');
    collection.remove({_id: req.param('id')}, {}, function (err, doc) {
        if (err) {
            res.send("Unable to remove");
        }
        else {
            res.redirect('/');
        }
    });
});

router.post('/addbook', function(req, res) {
    var db = req.db;
    var collection = db.get('library');
    collection.insert({
        author : req.body.author,
        title : req.body.title
    }, function (err, doc) {
        if (err) {
            res.send("Unable to insert");
        }
        else {
            res.redirect('/');
        }
    });
});

module.exports = router;